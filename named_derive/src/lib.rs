use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, DeriveInput};

#[proc_macro_derive(Named, attributes(name))]
pub fn derive_get_name(body: TokenStream) -> TokenStream {
    let ast = parse_macro_input!(body as DeriveInput);

    impl_get_name(&ast)
}

fn impl_get_name(input: &syn::DeriveInput) -> TokenStream {
    let name = &input.ident;

    if let syn::Data::Struct(struct_ast) = &input.data {
        // find field with name attribute
        let mut maybe_field_ident: Option<&syn::Ident> = None;
        for field in &struct_ast.fields {
            if field.attrs.iter().any(|attr| {
                attr.path()
                    .get_ident()
                    .map(|ident| ident == "name")
                    .is_some()
            }) {
                maybe_field_ident = field.ident.as_ref();
            }
        }
        if let Some(field_ident) = maybe_field_ident {
            quote! {
               impl Named for #name {
                   fn get_name(&self) -> &String {
                       &self.#field_ident
                   }
               }
            }
            .into()
        } else {
            // User must define the attribute that gets used as name
            panic!("#[derive(Named)] needs a field with #[name] attribute!");
        }
    } else {
        //Nope. This is an Enum. We cannot handle these!
        panic!("#[derive(Named)] is only defined for structs, not for enums!");
    }
}

trait Named {
    fn get_name(&self) -> &String;
}
