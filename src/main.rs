use std::{fs::File, process};

use clap::Parser;
use named_derive::Named;
use serde::Deserialize;

trait Named {
    fn get_name(&self) -> &String;
}

#[derive(Deserialize)]
#[serde(untagged)]
enum Package {
    Normal(String),
    Deprecated(DeprecatedPackage),
}

#[derive(Deserialize, Named)]
struct InternetPackage {
    #[serde(rename = "pkg-name")]
    #[name]
    pkg_name: String,
    command: String,
}

#[derive(Deserialize, Named)]
struct DeprecatedPackage {
    #[serde(rename = "pkg-name")]
    #[name]
    pkg_name: String,
    deprecated: String,
}
impl DeprecatedPackage {
    pub fn print_warnings(&self) {
        println!(
            "Deprecation notice ({}): {}",
            self.pkg_name, self.deprecated
        )
    }
}

#[derive(Deserialize, Named)]
struct PackageGroup {
    #[name]
    group: String,
    pkgs: Vec<Package>,
}

#[derive(Deserialize)]
#[serde(untagged)]
enum PackageGroupListItem {
    NormalPackage(String),
    DeprecatedPackage(DeprecatedPackage),
    PackageGroup(PackageGroup),
}

#[derive(Deserialize)]
struct PackageGroupList {
    pacman: Vec<PackageGroupListItem>,
    aur: Vec<PackageGroupListItem>,
    #[serde(rename = "from-internet")]
    from_internet: Vec<InternetPackage>,
}

#[derive(Deserialize)]
struct PackagesDocument {
    packages: PackageGroupList,
}

#[derive(Parser)]
#[command(author, version, about, long_about=None)]
struct Cli {
    /// Output file where binary is stored
    #[arg(short, long = "packages")]
    package_file_path: Option<std::path::PathBuf>,
    /// Enable debug output to stdout
    #[arg(short, long = "verbose")]
    debug_enable: bool,
}

fn main() {
    let cli = Cli::parse();
    let package_file_path = cli
        .package_file_path
        .unwrap_or("packages.yml".into())
        .canonicalize()
        .unwrap_or_else(|err| {
            eprintln!("Error: Could not find package file:");
            eprintln!("{err}");
            process::exit(1);
        });

    let package_file = File::open(package_file_path).unwrap_or_else(|err| {
        eprintln!("Error: Could not open package file:");
        eprintln!("{err}");
        process::exit(1);
    });
    let packages_document: PackagesDocument =
        serde_yaml::from_reader(package_file).unwrap_or_else(|err| {
            eprintln!("Error: Could not parse package file:");
            eprintln!("{err}");
            process::exit(1);
        });

    let mut install_string = String::new();

    for pkg_item in packages_document.packages.pacman {
        install_string.push(' ');
        install_string.push_str(
            match pkg_item {
                PackageGroupListItem::PackageGroup(PackageGroup { group, pkgs }) => pkgs
                    .iter()
                    .map(|pkg| {
                        match pkg {
                            Package::Normal(name) => name,
                            Package::Deprecated(dep_pkg) => {
                                dep_pkg.print_warnings();
                                dep_pkg.get_name()
                            }
                        }
                        .clone()
                    })
                    .collect::<Vec<String>>()
                    .join(" "),
                PackageGroupListItem::NormalPackage(name) => name,
                PackageGroupListItem::DeprecatedPackage(dep_pkg) => {
                    dep_pkg.print_warnings();
                    dep_pkg.get_name().clone()
                }
            }
            .as_str(),
        );
    }

    let install_command =
        String::from("pacman --noconfirm -Syu && pacman --noconfirm -S ") + install_string.as_str();

    println!();
    println!("#####################");
    println!("Installation command:");
    println!("#####################");
    println!("{install_command}");
    println!();
    println!("Use command for installation? [Y/n]: ");
}
